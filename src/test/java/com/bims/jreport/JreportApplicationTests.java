package com.bims.jreport;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSON;
import com.bims.jreport.pdf.CustomStyle;
import com.bims.jreport.pdf.builder.PdfBuilder;
import com.bims.jreport.utils.ReportUtil;
import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

@SpringBootTest
class JreportApplicationTests {

    @Test
    void contextLoads() throws IOException {

        System.out.println(1|2);
        String rgb = "rgb(21,43,43)";
        int[] ints = ReportUtil.subRgb(rgb);

        System.out.println(CharsetUtil.CHARSET_UTF_8.name());

        ArrayList<Integer> integers = CollUtil.newArrayList(1, 2);
        integers.clear();
        integers.add(22);
//        JSON.DEFAULT_TYPE_KEY="__t";
        CustomStyle signElement = new CustomStyle();
        signElement.setFontSize(22);



        String productFrameStr = JSON.toJSONString(signElement);
        System.out.println("序列化：" + productFrameStr);

        CustomStyle dataDownload = JSON.parseObject(productFrameStr, CustomStyle.class);
        System.out.println("反序列化："  + dataDownload);
        PdfBuilder pdfBuilder = new PdfBuilder(null,null,1);
        File file = new File("F:\\PDF\\test.pdf");
        File file2 = new File("F:\\PDF\\处罚建议书.pdf");

        PdfDocument pdfDocument = new PdfDocument(new PdfReader(file2),new PdfWriter(file));

        PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDocument ,true);
        PdfDocument document = pdfDocument.addNewPage().getDocument();
        System.out.println(pdfDocument.equals(document));
        System.out.println(pdfDocument.getNumberOfPages());
        pdfDocument.close();
    }

}
