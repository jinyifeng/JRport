package com.bims.jreport.document.fields;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocWatermarkField implements Serializable {
    private static final long serialVersionUID = -362275441677635716L;
}
