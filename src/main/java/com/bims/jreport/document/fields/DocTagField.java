package com.bims.jreport.document.fields;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocTagField implements Serializable {
    private static final long serialVersionUID = 232669905108717542L;


    private List<DocTagColumnField> columns;

    //边距
    private float margin;
}
