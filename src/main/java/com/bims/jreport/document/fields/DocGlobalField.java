package com.bims.jreport.document.fields;

import com.bims.jreport.pdf.CustomOption;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocGlobalField implements Serializable {
    private static final long serialVersionUID = 5721815446723334371L;

    //单元格自动高度
    private Boolean autoCellHeight;


}
