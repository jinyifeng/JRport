package com.bims.jreport.document.fields;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocTagColumnField implements Serializable {
    private static final long serialVersionUID = 232669905108717542L;

    //内容 变量 ${page_number}-当前页 ${total_page}-总页数
    private String content;

    //样式
    private String style;
}
