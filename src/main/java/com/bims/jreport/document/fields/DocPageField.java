package com.bims.jreport.document.fields;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocPageField implements Serializable {
    private static final long serialVersionUID = 6262425094729715767L;

    private String size;

    private String layout;

    private float[] margin;
}
