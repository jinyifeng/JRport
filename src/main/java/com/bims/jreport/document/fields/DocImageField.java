package com.bims.jreport.document.fields;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import com.bims.jreport.pdf.CustomImgData;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocImageField implements Serializable {
    private static final long serialVersionUID = 1839253775951354278L;

    //图片名称
    private String name;

    //图片URL
    private String url;

    //图片大小
    private int size;


    /**
     * 通过url获得图片对象
     *
     * @return
     */
    public CustomImgData fetchImageData() {
        byte[] imgBytes = HttpUtil.downloadBytes(this.url);
        ImageData imageData = ImageDataFactory.create(imgBytes, true);
        return new CustomImgData(name, imageData);
    }
}
