package com.bims.jreport.document.style;


import cn.hutool.core.util.StrUtil;
import com.bims.jreport.enums.FontStyleEnum;
import com.bims.jreport.pdf.CustomStyle;
import com.bims.jreport.utils.ReportUtil;
import com.itextpdf.io.font.constants.FontStyles;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocStyle implements Serializable {

    private static final long serialVersionUID = -2214271636943032460L;

    //文本 对齐方式 默认居中
    private String textAlign = TextAlignment.CENTER.name();

    //水平对齐方式
    //  cell.setProperty(VERTICAL_ALIGNMENT, VerticalAlignment.TOP);
    private String verticalAlign = VerticalAlignment.MIDDLE.name();

    //字体类型 名称
    private String fontFamily;

    //字体颜色
    private String color;

    //字体大小
    private int fontSize;

    //字体斜体
    private String fontStyle;

    //下划线
    private String textDecoration;

    //加粗
    private String fontWeight;

    //单元格背景颜色
    private String backgroundColor;

    //上边框
    private DocBorderStyle borderTop;

    //右边框
    private DocBorderStyle borderRight;

    //下边框
    private DocBorderStyle borderBottom;

    //左边框
    private DocBorderStyle borderLeft;


    /**
     * 获取itext 所需要的样式
     *
     * @return
     */
    public CustomStyle fetchStyle() {
        CustomStyle customStyle = new CustomStyle();
        TextAlignment textAlignment = TextAlignment.valueOf(this.textAlign.toUpperCase());
        VerticalAlignment verticalAlignment = VerticalAlignment.valueOf(this.verticalAlign.toUpperCase());

        customStyle.setAlignment(textAlignment);
        customStyle.setVerticalAlignment(verticalAlignment);
        customStyle.setFontSize(fontSize);
        //下划线
        customStyle.setUnderline(FontStyleEnum.UNDERLINE.name().equals(this.textDecoration.toUpperCase()));

        //字体颜色
        customStyle.setFontColor(ReportUtil.creatDeviceRgb(this.color));
        customStyle.setBackGroundColor(ReportUtil.creatDeviceRgb(this.backgroundColor));

        String fontName = this.fontFamily;
        //又粗又斜
        if (FontStyleEnum.BOLD.name().equals(this.textDecoration.toUpperCase()) && FontStyleEnum.ITALIC.name().equals(this.fontStyle.toUpperCase())) {
            fontName = fontName + StrUtil.UNDERLINE + FontStyles.BOLDITALIC;
        }else if(FontStyleEnum.BOLD.name().equals(this.textDecoration.toUpperCase())){
            //粗
            fontName = fontName + StrUtil.UNDERLINE + FontStyles.BOLD;
        }else if(FontStyleEnum.ITALIC.name().equals(this.fontStyle.toUpperCase())){
            //斜
            fontName = fontName + StrUtil.UNDERLINE + FontStyles.ITALIC;
        }
        //字体名称 粗 斜体 格式 字体名称_FontStyles.BOLD
        customStyle.setFontName(fontName);
        //上下左右边框
        customStyle.setBorderBottom(this.borderBottom.fetchBorder());
        customStyle.setBorderLeft(this.borderLeft.fetchBorder());
        customStyle.setBorderRight(this.borderRight.fetchBorder());
        customStyle.setBorderTop(this.borderTop.fetchBorder());

        return customStyle;
    }


}
