package com.bims.jreport.document.style;


import com.bims.jreport.enums.BorderStyleEnum;
import com.bims.jreport.utils.ReportUtil;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DoubleBorder;
import com.itextpdf.layout.borders.SolidBorder;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
public class DocBorderStyle implements Serializable {

    private static final long serialVersionUID = 6305538363613078725L;

    //边框宽度
    private float borderWidth;

    //边框颜色
    private String borderColor;

    //边框类别
    private String borderStyle;


    /**
     * 转换border对象
     * @return
     */
    public Border fetchBorder() {
        DeviceRgb color = ReportUtil.creatDeviceRgb(this.borderColor);
        Border border = null;
        switch (BorderStyleEnum.valueOf(this.borderStyle.toUpperCase())) {
            case SOLID:
                border = new SolidBorder(color, borderWidth);
                break;
            case DOUBLE:
                border = new DoubleBorder(color, borderWidth);
                break;
            default:
                //TODO 抛出异常
        }
        return border;
    }
}
