package com.bims.jreport.document.table.cell.content;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.content.basic.BindContent;
import com.bims.jreport.document.table.cell.content.basic.PlainContent;
import com.bims.jreport.document.table.cell.content.basic.RefContent;
import com.bims.jreport.enums.CellTypeEnum;
import com.bims.jreport.pdf.builder.BuilderContext;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * @author wangrenjie
 */
@Getter
@Setter
@JSONType(seeAlso = {PlainContent.class, BindContent.class, RefContent.class})
public class BaseContent implements Serializable {

    private static final long serialVersionUID = 3480941816186306471L;

    private String value;

    @JsonIgnore
    private String style;

    @JsonIgnore
    private CellTypeEnum cellType;


    /**
     * 是否展开
     * @return
     */
    public boolean isExpand(){
        return false;
    }

    /**
     * 处理单元格内容
     *
     * @param context     上下文
     * @param cell        单元格
     * @param expandIndex 展开索引
     * @param pageIndex   当前页
     */
    public void fetchContent(BuilderContext context, Cell cell, Integer expandIndex, int pageIndex) {
        cell.add(new Paragraph(this.value));
    }



}
