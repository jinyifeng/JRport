package com.bims.jreport.document.table.setting;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class BackgroundSetting implements Serializable {
    private static final long serialVersionUID = 232669905108717542L;

    //是否可打印？
    private boolean printable;

    //背景设置，图片默认为backgroud引用
    private String image;


}
