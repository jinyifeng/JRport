package com.bims.jreport.document.table;

import com.bims.jreport.document.table.cell.BaseCell;
import com.bims.jreport.document.table.setting.BackgroundSetting;
import com.bims.jreport.document.table.setting.ExpandSetting;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class DocTable implements Serializable {
    private static final long serialVersionUID = 232669905108717542L;

    private String name;

    private String title;

    //子表单设置
    private Map<String, ExpandSetting> subform;

    //列宽
    private float[] cols;

    //行高
    private float[] rows;

    //背景设置
    private BackgroundSetting backgroundSetting;

    //单元格
    private BaseCell[][] cells;

    //悬浮？
    @Deprecated
    private int[] suspensions;

    //重复？
    @Deprecated
    private int[] repeat;




}
