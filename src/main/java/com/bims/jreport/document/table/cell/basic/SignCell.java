package com.bims.jreport.document.table.cell.basic;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.BaseCell;
import com.bims.jreport.enums.CellTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import static com.bims.jreport.constant.ReportConstants.CUSTOMER_CELL_TYPE_SIGN;

/**
 * @author wangrenjie
 */
@Getter
@Setter
@JSONType(typeName = CUSTOMER_CELL_TYPE_SIGN)
public class SignCell extends BaseCell implements Serializable {

    private static final long serialVersionUID = 3723190899025568830L;



    @Override
    public CellTypeEnum getCellType(){
        return CellTypeEnum.SIGN;
    }
}
