package com.bims.jreport.document.table.cell.content.basic;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.content.BaseFieldContent;
import com.bims.jreport.enums.BindTypeEnum;
import com.bims.jreport.enums.CellTypeEnum;
import com.bims.jreport.pdf.builder.BuilderContext;
import com.bims.jreport.pdf.fields.BasePdfFormField;
import com.bims.jreport.pdf.fields.SignPdfFormField;
import com.bims.jreport.pdf.fields.TextPdfFormField;
import com.bims.jreport.utils.AssertUtil;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.layout.element.Cell;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

import static com.bims.jreport.constant.ReportConstants.CUSTOMER_CONTENT_TYPE_BIND;
import static com.bims.jreport.constant.ReportConstants.EXPAND_TAG;
import static com.bims.jreport.constant.ReportErrorCodeConstant.PDF_TEMP_BIND_TYPE_ILLEGAL;


/**
 * 绑定类型，包含：form、workflow、exp ,exp不开发
 *
 * @author wangrenjie
 */
@Getter
@Setter
@Slf4j
@JSONType(typeName = CUSTOMER_CONTENT_TYPE_BIND)
public class BindContent extends BaseFieldContent implements Serializable {

    private static final long serialVersionUID = 3723190899025568830L;

    //是否是多行文本
    private boolean multiline;

    /**
     * 处理单元格内容
     *
     * @param context     上下文
     * @param cell        单元格
     * @param expandIndex 展开索引
     * @param pageIndex   当前页
     */
    @Override
    public void fetchContent(BuilderContext context, Cell cell, Integer expandIndex, int pageIndex) {
        String valueType = this.getValueType();
        //获取最后一次调用{@link #layout(LayoutContext)}方法后得到的占用区域。
        String fieldName = IdUtil.simpleUUID();
        String bindName = this.isExpand() ?
                StrUtil.builder(this.getValue(), EXPAND_TAG, expandIndex.toString(), StrUtil.COMMA, String.valueOf(pageIndex)).toString() :
                StrUtil.builder(this.getValue(), StrUtil.COMMA, String.valueOf(pageIndex)).toString();
        Rectangle rect = cell.getRenderer().getOccupiedArea().getBBox();
        BasePdfFormField formField = null;
        switch (BindTypeEnum.valueOf(valueType)) {
            case Form:
                formField = new TextPdfFormField(rect, fieldName, this.getStyle(), multiline);
                break;
            case Workflow:
                //工作流内容，签名单元格属性
                if (CellTypeEnum.SIGN.equals(this.getCellType())) {
                    formField = new SignPdfFormField(rect, fieldName, this.getStyle());
                } else {
                    formField = new TextPdfFormField(rect, fieldName, this.getStyle(), multiline);
                }
                break;
            default:
                log.error("not found this bind type {}", valueType);
                AssertUtil.error(PDF_TEMP_BIND_TYPE_ILLEGAL);

        }
        context.addDataBind(fieldName, bindName);
        context.addField(pageIndex, formField);
    }

    /**
     * 是否展开
     *
     * @return
     */
    @Override
    public boolean isExpand() {
        return this.getValue().contains(StrUtil.DOT);
    }


}
