package com.bims.jreport.document.table.cell.content.basic;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.content.BaseContent;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import static com.bims.jreport.constant.ReportConstants.CUSTOMER_CONTENT_TYPE_PLAIN;


/**
 * 可以理解为文本，即输入是啥就显示啥，默认类型，可以省略
 * @author wangrenjie
 */
@Getter
@Setter
@JSONType(typeName = CUSTOMER_CONTENT_TYPE_PLAIN)
public class PlainContent extends BaseContent implements Serializable {

    private static final long serialVersionUID = 3723190899025568830L;

}
