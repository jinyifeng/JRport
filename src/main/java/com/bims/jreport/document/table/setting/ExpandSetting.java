package com.bims.jreport.document.table.setting;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class ExpandSetting implements Serializable {
    private static final long serialVersionUID = 232669905108717542L;


    //最大行数
    private int maxPerPage;


}
