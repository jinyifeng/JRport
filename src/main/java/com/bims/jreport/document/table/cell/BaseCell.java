package com.bims.jreport.document.table.cell;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.basic.*;
import com.bims.jreport.document.table.cell.content.BaseContent;
import com.bims.jreport.pdf.CustomStyle;
import com.bims.jreport.enums.CellTypeEnum;
import com.itextpdf.layout.element.Cell;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

import static com.bims.jreport.constant.ReportConstants.DEFAULT_CELL_RS_CS;
import static com.bims.jreport.constant.ReportConstants.DEFAULT_STYLE_NAME;

/**
 * @author wangrenjie
 */
@Getter
@Setter
@JSONType(seeAlso = {ImageCell.class, FieldCell.class, FileCell.class, TextCell.class, SignCell.class, QrCell.class, InnerFormCell.class})
public abstract class BaseCell implements Serializable {

    private static final long serialVersionUID = 1234309516478994210L;

    //合并行
    private int rs;
    //合并列
    private int cs;
    //引用样式 --> 默认样式
    private String style = DEFAULT_STYLE_NAME;
    //内容
    private BaseContent content;


    /**
     * 获得单元格类别
     *
     * @return 单元格类别
     */
    public CellTypeEnum getCellType() {
        return CellTypeEnum.TEXT;
    }

    /**
     * 继承父单元格部分内容
     *
     * @return
     */
    public BaseContent getContent() {
        if (null != this.content) {
            this.content.setStyle(this.style);
            this.content.setCellType(this.getCellType());
        }
        return this.content;
    }


    /**
     * 获取样式
     * TODO  默认样式处理
     *
     * @param styleSet 样式集
     * @return
     */
    public CustomStyle getCellStyle(Map<String, CustomStyle> styleSet) {
        return styleSet.get(this.style);
    }


    /**
     * 转换baseCell为Itext对象
     *
     * @param styleSet 样式集
     * @return itext 单元格
     */
    public Cell convertCell(Map<String, CustomStyle> styleSet) {
        Cell cell = new Cell(this.rs, this.cs);
        //添加单元格样式
        this.addCellStyle(this.getCellStyle(styleSet), cell);
        return cell;
    }

    /**
     * 添加单元格样式
     *
     * @param cellStyle
     * @param cell
     */
    public void addCellStyle(CustomStyle cellStyle, Cell cell) {
        //文本对齐方式
        cell.setTextAlignment(cellStyle.getAlignment())
                //字体
                .setFontFamily(cellStyle.getFontName())
                .setFontSize(cellStyle.getFontSize())
                .setFontColor(cellStyle.getFontColor())
                //单元格背景颜色
                .setBackgroundColor(cellStyle.getBackGroundColor())
                //边框
                .setBorderTop(cellStyle.getBorderTop())
                .setBorderBottom(cellStyle.getBorderBottom())
                .setBorderLeft(cellStyle.getBorderLeft())
                .setBorderRight(cellStyle.getBorderRight())
        ;
        if (cellStyle.isUnderline()) {
            cell.setUnderline();
        }
    }


    /**
     * 是否需要向下展开
     *
     * @return
     */
    public boolean needExpand() {
        return false;
    }

    /**
     * 是否需要跳过绘制此单元格
     * 针对合并
     *
     * @return
     */
    public boolean isSkip() {
        return this.rs + this.cs <= DEFAULT_CELL_RS_CS;
    }

}
