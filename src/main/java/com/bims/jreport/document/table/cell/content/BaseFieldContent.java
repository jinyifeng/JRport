package com.bims.jreport.document.table.cell.content;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * @author wangrenjie
 */
@Getter
@Setter
public abstract class BaseFieldContent extends BaseContent implements Serializable {

    private static final long serialVersionUID = 3480941816186306471L;

    //值类别
    private String valueType;
}
