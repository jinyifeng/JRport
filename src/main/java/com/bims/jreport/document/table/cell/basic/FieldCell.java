package com.bims.jreport.document.table.cell.basic;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.BaseCell;
import com.bims.jreport.document.table.cell.content.basic.BindContent;
import com.bims.jreport.enums.CellTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import static com.bims.jreport.constant.ReportConstants.CUSTOMER_CELL_TYPE_FIELD;

/**
 * 表单域生成规则  组件ID表达式_EXPAND_1,pageIndex
 * 例如 f16.f12_EXPAND_1,2
 * @author wangrenjie
 */
@Getter
@Setter
@JSONType(typeName = CUSTOMER_CELL_TYPE_FIELD)
public class FieldCell extends BaseCell implements Serializable {

    private static final long serialVersionUID = 3723190899025568830L;


    @Override
    public CellTypeEnum getCellType(){
        return CellTypeEnum.FIELD;
    }

    @Override
    public boolean needExpand(){
        //单元格内容是绑定类型 并且单元格值包含 “.” 是子表单类型 需要向下展开
        return this.getContent() instanceof BindContent && this.getContent().isExpand();
    }

}
