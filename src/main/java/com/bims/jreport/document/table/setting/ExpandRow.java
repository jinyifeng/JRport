package com.bims.jreport.document.table.setting;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.bims.jreport.document.table.cell.BaseCell;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ExpandRow implements Serializable {
    private static final long serialVersionUID = -3685699699511008080L;


    private int expandNum;

    private int colNum;

    private float rowHeight;

    private BaseCell baseCell;


    /**
     * @param colNum        第几列
     * @param baseCell      单元格
     * @param expandNum     需要展开几行 当前行已经填充所以 -1
     */
    public ExpandRow(int colNum, BaseCell baseCell,  int expandNum,float rowHeight) {
        this.colNum = colNum;
        this.baseCell = baseCell;
        this.rowHeight = rowHeight;
        this.expandNum = expandNum;
    }



    public float[] getRowHeights(int currentRow, int rowNum, float height) {
        int differ = this.getExpandNum() - 1 + currentRow - rowNum;
        if (differ > 0) {
            float[] rowHeights = new float[differ];
            for (int i = 0; i <= differ; i++) {
                rowHeights[i] = height;
            }
            return rowHeights;
        }
        return null;
    }
}
