package com.bims.jreport.document.table.cell.content.basic;

import com.alibaba.fastjson.annotation.JSONType;
import com.bims.jreport.document.table.cell.content.BaseFieldContent;
import com.bims.jreport.enums.RefTypeEnum;
import com.bims.jreport.pdf.CustomImgData;
import com.bims.jreport.pdf.builder.BuilderContext;
import com.bims.jreport.utils.AssertUtil;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

import static com.bims.jreport.constant.ReportConstants.CUSTOMER_CONTENT_TYPE_REF;
import static com.bims.jreport.constant.ReportErrorCodeConstant.PDF_TEMP_REF_TYPE_ILLEGAL;

/**
 * @author wangrenjie
 * 引用类型，包含：image、sheet，sheet暂不开发
 */
@Getter
@Setter
@Slf4j
@JSONType(typeName = CUSTOMER_CONTENT_TYPE_REF)
public class RefContent extends BaseFieldContent implements Serializable {

    private static final long serialVersionUID = 3723190899025568830L;


    /**
     * 处理单元格内容
     *
     * @param context     上下文
     * @param cell        单元格
     * @param expandIndex 展开索引
     * @param pageIndex   当前页
     */
    @Override
    public void fetchContent(BuilderContext context, Cell cell, Integer expandIndex, int pageIndex) {
        switch (RefTypeEnum.valueOf(this.getValueType())) {
            case Image:
                CustomImgData imageData = context.getImageData(this.getValue());
                cell.add(new Image(imageData.getData()));
                break;
            case Sheet:
                break;
            default:
                log.error("not found this ref type {}", this.getValueType());
                AssertUtil.error(PDF_TEMP_REF_TYPE_ILLEGAL);
        }
    }

}
