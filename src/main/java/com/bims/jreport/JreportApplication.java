package com.bims.jreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication(scanBasePackages = "com.bims.jreport.*")
public class JreportApplication {

    public static void main(String[] args) {
        SpringApplication.run(JreportApplication.class, args);
    }
}
