/**
 *
 */
package com.bims.jreport.constant;


import cn.hutool.core.map.MapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xuyang
 */
public class ReportConstants {
    public static final Integer DEFAULT_START_INDEX_FOR_PAGING = 1;
    public static final Integer DEFAULT_PAGE_SIZE_FOR_PAGING = 20;


    /**
     * 默认属性
     */
    public static final int DEFAULT_PDF_PAGE_INDEX = 1;

    public static final int DEFAULT_CELL_RS_CS = 1;


    public static final int EXPAND_START_INDEX = 0;


    public static final String EXPAND_TAG = "_EXPAND_";

    public static final int CELL_START_COL_NUM = 1;

    public static final int CELL_START_ROW_NUM = 1;

    public static final String DEFAULT_STYLE_NAME = "DEFAULT_STYLE";



    //DPF/A所必须包含的颜色配置文件
    public static final String INTENT = "depot/sRGB_CS_profile.icm";

    /**
     * 单元格
     */
    public static final String CUSTOMER_CELL_TYPE_IMAGE = "CL_IMAGE";
    public static final String CUSTOMER_CELL_TYPE_TEXT = "CL_TEXT";
    public static final String CUSTOMER_CELL_TYPE_QR = "CL_QR";
    public static final String CUSTOMER_CELL_TYPE_SIGN = "CL_SIGN";
    public static final String CUSTOMER_CELL_TYPE_FIELD = "CL_FIELD";
    public static final String CUSTOMER_CELL_TYPE_FILE = "CL_FILE";
    public static final String CUSTOMER_CELL_TYPE_INNER_FORM = "CL_INNER_FORM";


    /**
     * 内容
     */
    public static final String CUSTOMER_CONTENT_TYPE_PLAIN = "CT_PLAIN";
    public static final String CUSTOMER_CONTENT_TYPE_REF = "CT_REF";
    public static final String CUSTOMER_CONTENT_TYPE_BIND = "CT_BIND";

    public static final Map<String, String> FONT_MAP = MapUtil.builder(new HashMap<String, String>()).put("s","s").map();


}