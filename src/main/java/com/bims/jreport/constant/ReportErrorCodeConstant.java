package com.bims.jreport.constant;

public class ReportErrorCodeConstant {

    public static final String COMMON_GENERATE_ERROR = "000001";
    public static final String COMMON_REQUEST_MISSING_PARAM = "000002";
    public static final String COMMON_REQUEST_ARGUMENT_NOT_VALID = "000003";
    //非法的缓存名称
    public static final String  COMMON_CACHE_ILLEGAL_KEY = "000010";
    //字体缓存创建失败
    public static final String  COMMON_CACHE_FONT_ERROR = "000011";

    //页边距参数错误
    public static final String  PDF_MARGINS_LENGTH_ERROR = "100002";

    //错误的绑定类别
    public static final String  PDF_TEMP_BIND_TYPE_ILLEGAL = "100010";
    //错误的引用类别
    public static final String  PDF_TEMP_REF_TYPE_ILLEGAL = "100011";

}
