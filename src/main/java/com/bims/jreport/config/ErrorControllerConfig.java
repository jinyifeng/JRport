package com.bims.jreport.config;

import com.bims.jreport.exception.handler.ReportErrorController;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class ErrorControllerConfig {

    private final ServerProperties serverProperties;

    private final List<ErrorViewResolver> errorViewResolvers;

    public ErrorControllerConfig(ServerProperties serverProperties,
                                 ObjectProvider<List<ErrorViewResolver>> errorViewResolversProvider) {
        this.serverProperties = serverProperties;
        this.errorViewResolvers = errorViewResolversProvider.getIfAvailable();
    }

    /**
     * 抄的是{@link ErrorMvcAutoConfiguration#basicErrorController(ErrorAttributes)}
     *
     * @param errorAttributes
     * @return
     */
    @Bean
    public ReportErrorController customErrorController(ErrorAttributes errorAttributes) {
        return new ReportErrorController(
                errorAttributes,
                this.serverProperties.getError(),
                this.errorViewResolvers
        );
    }

}