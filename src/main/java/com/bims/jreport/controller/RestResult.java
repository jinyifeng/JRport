package com.bims.jreport.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;



@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestResult<T> {
	public static final int STATUS_SUCCESS = 1;
	public static final int STATUS_FAILED = 0;
	private int status;
	private T data;
	private String message;
	private String errorCode;

	/**
	 * 实例化一个正确的返回
	 * @param data
	 * @return
	 */
	public static RestResult newSuccessIntance(Object data) {
		RestResult response = new RestResult();
		response.setData(data);
		response.setStatus(STATUS_SUCCESS);
		return response;
	}

	/**
	 * 实例化一个错误的返回
	 * @param message
	 * @return
	 */
	public static RestResult newFailedInstance(String message) {
		RestResult response = new RestResult();
		response.setStatus(STATUS_FAILED);
		response.setMessage(message);
		return response;
	}

	/**
	 * 返回错误编码和错误信息
	 * @param errorCode
	 * @param message
	 * @return
	 */
	public static RestResult newFailedInstance(String errorCode, String message) {
		RestResult response = new RestResult();
		response.setStatus(STATUS_FAILED);
		response.setMessage(message);
		response.setErrorCode(errorCode);
		return response;
	}

	/**
	 * 返回错误编码和错误信息
	 * @param errorCode
	 * @param message
	 * @return
	 */
	public static RestResult newFailedInstance(String errorCode, String message, Object data) {
		RestResult response = new RestResult();
		response.setStatus(STATUS_FAILED);
		response.setMessage(message);
		response.setErrorCode(errorCode);
		response.setData(data);
		return response;
	}
}
