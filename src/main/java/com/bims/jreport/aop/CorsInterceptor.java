package com.bims.jreport.aop;


import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 跨域
 */
public class CorsInterceptor extends HandlerInterceptorAdapter {

    public static final String CREDENTIALS_NAME = "Access-Control-Allow-Credentials";
    public static final String ORIGIN_NAME = "Access-Control-Allow-Origin";
    public static final String METHODS_NAME = "Access-Control-Allow-Methods";
    public static final String HEADERS_NAME = "Access-Control-Allow-Headers";
    public static final String MAX_AGE_NAME = "Access-Control-Max-Age";
    public static final String EXPOSE_HEADERS = "Access-Control-Expose-Headers";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        response.setHeader(CREDENTIALS_NAME, "true");
        response.setHeader(ORIGIN_NAME, request.getHeader("Origin"));
        response.setHeader(METHODS_NAME, "GET,OPTIONS,POST,PUT,DELETE");
        response.setHeader(HEADERS_NAME, "Origin,X-Requested-With,Content-Type,Accept,Authorization,X-SYS-ID,X-ORG-ID,X-MENU,token,X-SIGN,X-NONCE");
        response.setHeader(EXPOSE_HEADERS, "token,Content-Disposition");
        response.setHeader(MAX_AGE_NAME, "3600");
        return true;
    }

}