package com.bims.jreport.cache;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.StrUtil;
import com.bims.jreport.utils.AssertUtil;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.FontStyles;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.bims.jreport.cache.CustomCache.FONT_CACHE;
import static com.bims.jreport.constant.ReportConstants.FONT_MAP;
import static com.bims.jreport.constant.ReportErrorCodeConstant.COMMON_CACHE_FONT_ERROR;

/**
 * @author wangrenjie
 */
@Slf4j
@Component(FONT_CACHE)
public class FontCache extends BaseCache<String, PdfFont> {

    private static final long serialVersionUID = -151463451545675612L;


    @Override
    public void init() {
        FONT_MAP.forEach((k, v) -> {
            try {
                String path = ResourceUtil.getResource(v).getPath();
                //正常
                PdfFont font = PdfFontFactory.createRegisteredFont(path, PdfEncodings.IDENTITY_H, true, true);
                this.put(k, font);
                String fontName = StrUtil.builder(k + StrUtil.UNDERLINE).toString();
                //斜体
                PdfFont italicFont = PdfFontFactory.createRegisteredFont(path, PdfEncodings.IDENTITY_H, true, FontStyles.ITALIC, true);
                this.put(fontName + FontStyles.ITALIC, italicFont);
                //粗体
                PdfFont boldFont = PdfFontFactory.createRegisteredFont(path, PdfEncodings.IDENTITY_H, true, FontStyles.BOLD, true);
                this.put(fontName + FontStyles.BOLD, boldFont);
                //粗图&斜体
                PdfFont boldItalicFont = PdfFontFactory.createRegisteredFont(path, PdfEncodings.IDENTITY_H, true, FontStyles.BOLDITALIC, true);
                this.put(fontName + FontStyles.BOLDITALIC, boldItalicFont);

            } catch (IOException e) {
                log.error("font creat error is  ,{}", e.getLocalizedMessage());
                AssertUtil.error(COMMON_CACHE_FONT_ERROR);
            }
        });
    }


    /**
     * 检查Key是否合法
     *
     * @return 是否合法
     */
    @Override
    public boolean checkKey(String key) {
        return FONT_MAP.containsKey(key);
    }


}
