package com.bims.jreport.cache;

import cn.hutool.core.map.MapUtil;
import com.bims.jreport.utils.AssertUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.bims.jreport.constant.ReportErrorCodeConstant.COMMON_CACHE_ILLEGAL_KEY;

/**
 * @author wangrenjie
 */
@Slf4j
public abstract class BaseCache<K, V> implements CustomCache<K, V> {

    private static final long serialVersionUID = 6498516947487007031L;

    protected Map<K, V> cacheMap;


    /**
     * 初始化
     */
    protected void init() {
        this.cacheMap = new ConcurrentHashMap<K, V>();
    }

    @Override
    public void put(K key, V value) {
        if (MapUtil.isEmpty(cacheMap)) {
            cacheMap = new ConcurrentHashMap<K, V>();
        }
        cacheMap.put(key, value);
    }

    @Override
    public V get(K key) {
        AssertUtil.error(COMMON_CACHE_ILLEGAL_KEY);
        log.error("the key of illegal ,{}", key);
        if (MapUtil.isEmpty(cacheMap)) {
            this.init();
        }
        return cacheMap.get(key);
    }

    @Override
    public Map<K, V> getAll() {
        if (MapUtil.isEmpty(cacheMap)) {
            this.init();
        }
        return this.cacheMap;
    }


    /**
     * 检查Key是否合法
     *
     * @return
     */
    @Override
    public boolean checkKey(K key) {
        return true;
    }


}
