package com.bims.jreport.cache;

import cn.hutool.cache.impl.CacheObj;

import java.io.Serializable;
import java.util.Map;

/**
 * @author wangrenjie
 */
public interface CustomCache<K, V> extends Serializable {

    String FONT_CACHE = "fontCache";

    void put(K key, V value);

    V get(K key);

    Map<K, V> getAll();

    boolean checkKey(K key);
}
