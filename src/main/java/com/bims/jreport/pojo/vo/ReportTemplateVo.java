package com.bims.jreport.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportTemplateVo implements Serializable {
    private static final long serialVersionUID = -4747650221457988560L;
}
