package com.bims.jreport.pojo.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author wangrenjie
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportTemplateBo implements Serializable {
    private static final long serialVersionUID = -64917938921623733L;

    private UUID id;

    //名称
    private String name;

    //表单的ID
    private UUID formId;

    //模板JSON
    private String template;

    //版本
    private int ver;

    //是否启用
    private boolean enable;

    //创建人ID
    private UUID creatUserId;
}
