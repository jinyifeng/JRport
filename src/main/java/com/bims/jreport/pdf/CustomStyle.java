package com.bims.jreport.pdf;


import cn.hutool.core.util.StrUtil;
import com.bims.jreport.pdf.builder.BuilderContext;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class CustomStyle implements Serializable {

    private static final long serialVersionUID = -2214271636943032460L;

    //文本 对齐方式 默认居中
    private TextAlignment alignment;

    //水平对齐方式
    private VerticalAlignment verticalAlignment;

    //字体类型
    private String fontName;

    //字体颜色
    private Color fontColor;

    //字体大小
    private int fontSize;

    //下划线
    private boolean underline;

    //单元格背景颜色
    private Color backGroundColor;

    //上边框
    private Border borderTop;

    //右边框
    private Border borderRight;

    //下边框
    private Border borderBottom;

    //左边框
    private Border borderLeft;

//                cell.setProperty(VERTICAL_ALIGNMENT, VerticalAlignment.TOP);


}
