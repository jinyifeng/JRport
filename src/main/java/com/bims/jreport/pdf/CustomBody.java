package com.bims.jreport.pdf;

import com.bims.jreport.document.table.setting.BackgroundSetting;
import com.itextpdf.layout.element.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */

@Getter
@Setter
@AllArgsConstructor
public class CustomBody implements Serializable {

    private static final long serialVersionUID = -5004531990438894228L;

    //表格主体
    private Table table;

    //背景设置
    private BackgroundSetting backgroundSetting;
}
