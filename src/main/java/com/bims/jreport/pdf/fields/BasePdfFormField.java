package com.bims.jreport.pdf.fields;

import com.bims.jreport.pdf.CustomStyle;
import com.bims.jreport.pdf.builder.BuilderContext;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
@AllArgsConstructor
public abstract class BasePdfFormField implements Serializable {

    private static final long serialVersionUID = -1493166515979394038L;

    //坐标
    private Rectangle rectangle;

    //表单域名称
    private String fieldName;

    //样式
    private String style;


    /**
     * 获取表单域
     *
     * @param document 文档
     * @param context  上下文
     * @return 绘制用的表单域
     */
    public abstract PdfFormField fetchFormField(PdfDocument document, BuilderContext context);

    /**
     * 添加样式
     * @param formField 表单域
     * @param context 上下文
     */
    protected void addStyle(PdfFormField formField, BuilderContext context) {
        CustomStyle style = context.getStyle(this.getStyle());
        PdfFont font = context.getFont(style.getFontName());
        formField.setFont(font);
        formField.setFontSize(style.getFontSize());
        formField.setColor(style.getFontColor());
    }

}
