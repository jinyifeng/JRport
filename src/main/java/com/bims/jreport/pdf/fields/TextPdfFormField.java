package com.bims.jreport.pdf.fields;

import com.bims.jreport.pdf.builder.BuilderContext;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.forms.fields.PdfTextFormField;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class TextPdfFormField extends BasePdfFormField implements Serializable {


    private static final long serialVersionUID = -9156470834300190043L;

    //是否是多行文本
    private boolean multiline;

    public TextPdfFormField(Rectangle rectangle, String fieldName, String style, boolean multiline) {
        super(rectangle, fieldName,style);
        this.multiline = multiline;
    }


    @Override
    public PdfFormField fetchFormField(PdfDocument document, BuilderContext context) {
        PdfTextFormField formField = PdfFormField.createText(document, this.getRectangle(), this.getFieldName());
        formField.setMultiline(multiline);
        this.addStyle(formField,context);
        return formField;
    }
}
