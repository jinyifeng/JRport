package com.bims.jreport.pdf.fields;

import com.bims.jreport.pdf.builder.BuilderContext;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.forms.fields.PdfSignatureFormField;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author wangrenjie
 */
@Getter
@Setter
public class SignPdfFormField extends BasePdfFormField implements Serializable {

    private static final long serialVersionUID = -9156470834300190043L;


    public SignPdfFormField(Rectangle rectangle, String fieldName, String style) {
        super(rectangle, fieldName,style);
    }


    @Override
    public PdfFormField fetchFormField(PdfDocument document, BuilderContext context) {
        PdfSignatureFormField formField = PdfFormField.createSignature(document, this.getRectangle());
        formField.setFieldName(this.getFieldName());
        this.addStyle(formField,context);
        return formField;
    }
}
