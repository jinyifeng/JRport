package com.bims.jreport.pdf;


import com.itextpdf.kernel.geom.PageSize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


/**
 * pdf全局设置
 * @author wangrenjie
 */
@Getter
@Setter
@AllArgsConstructor
public class CustomOption {

    //单元格自动高度 ？无法实现
    @Deprecated
    private Boolean autoCellHeight;


}
