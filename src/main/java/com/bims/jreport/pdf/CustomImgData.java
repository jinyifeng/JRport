package com.bims.jreport.pdf;

import com.itextpdf.io.image.ImageData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author wangrenjie
 */
@Getter
@Setter
@AllArgsConstructor
public class CustomImgData {

    //可能以后会用到 预留
    private String name;

    private ImageData data;
}
