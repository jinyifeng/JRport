package com.bims.jreport.pdf.builder;

import com.bims.jreport.pdf.CustomOption;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.OutputStream;

/**
 * @author wangrenjie
 */
@Getter
@AllArgsConstructor
public abstract class BaseBuilder {

    //纸张大小
    private PageSize pageSize;

    //全局设置
    private CustomOption customOption;

    //上下文
    private BuilderContext context;


    /**
     * 页面边距设置
     * margins： [0]upper margin [1]right margin [2]the left margin [3]the lower margin
     *
     * @return this
     */
    abstract BaseBuilder margin(float[] margins);


    /**
     * 写入页眉
     *
     * @return this
     */
    abstract BaseBuilder header();

    /**
     * 写入页脚
     *
     * @return this
     */
    abstract BaseBuilder footer();

    /**
     * 写入水印
     *
     * @return this
     */
    abstract BaseBuilder watermark();

    /**
     * 输出文档对象
     *
     * @return 文档对象
     */
    abstract PdfDocument build();


    /**
     * 输出文件流对象
     *
     * @return 文件输出流
     */
    abstract OutputStream buildStream();
}
