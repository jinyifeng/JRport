package com.bims.jreport.pdf.builder;

import com.bims.jreport.pdf.CustomBody;
import com.bims.jreport.pdf.CustomOption;
import com.bims.jreport.pdf.fields.BasePdfFormField;
import com.bims.jreport.utils.AssertUtil;
import com.bims.jreport.utils.DocumentUtil;
import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.layout.Document;
import com.itextpdf.pdfa.PdfADocument;
import lombok.extern.slf4j.Slf4j;

import java.io.OutputStream;
import java.util.Collection;

import static com.bims.jreport.constant.ReportConstants.DEFAULT_PDF_PAGE_INDEX;
import static com.bims.jreport.constant.ReportErrorCodeConstant.PDF_MARGINS_LENGTH_ERROR;

/**
 * PDF构建
 * 目前只生成PDF/A模式
 *
 * @author wangrenjie
 */

@Slf4j
public class PdfBuilder extends BaseBuilder {

    //文档DOM
    private Document document;

    //当前页码
    private int pageIndex;

    /**
     * 构造
     *
     * @param pageSize 纸张属性
     */
    public PdfBuilder(PageSize pageSize, CustomOption customOption, BuilderContext context) {
        this(pageSize, customOption, context, DEFAULT_PDF_PAGE_INDEX);
    }

    public PdfBuilder(PageSize pageSize, BuilderContext context, int pageIndex) {
        this(pageSize, null, context, pageIndex);
    }

    public PdfBuilder(PageSize pageSize, CustomOption customOption, BuilderContext context, int pageIndex) {
        super(pageSize, customOption, context);
        this.pageIndex = pageIndex;
        PdfADocument pdfDocument = DocumentUtil.creatPdfDocument();
        this.document = new Document(pdfDocument, pageSize);
        //设置字体集
        document.setFontProvider(context.fetchFontProvider());

    }


    /**
     * 页面边距设置
     * margins： [0]upper margin [1]right margin [2]the left margin [3]the lower margin
     *
     * @return this
     */
    @Override
    public PdfBuilder margin(float[] margins) {
        AssertUtil.check(margins.length != 4, PDF_MARGINS_LENGTH_ERROR);
        log.error("margins  length is wrong, length  must 4");
        this.document.setMargins(margins[0], margins[1], margins[2], margins[3]);
        return this;
    }

    /**
     * 写入表单域
     *
     * @param formFields
     * @return this
     */
    public PdfBuilder form(Collection<BasePdfFormField> formFields) {
        PdfDocument pdfDocument = this.document.getPdfDocument();
        PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDocument, true);
        PdfPage page = pdfDocument.getPage(this.pageIndex);
        formFields.forEach(f -> form.addField(f.fetchFormField(pdfDocument, this.getContext()), page));
        return this;
    }


    /**
     * 写入页眉
     *
     * @return this
     */
    @Override
    public PdfBuilder header() {
        //TODO 待完善
        return this;
    }




    /**
     * 写入页脚
     *
     * @return this
     */
    @Override
    public PdfBuilder footer() {
        //TODO 待完善
        return this;
    }


    /**
     * 写入水印
     *
     * @return this
     */
    @Override
    public PdfBuilder watermark() {
        //TODO 待完善
        return this;
    }



    /**
     * 写入主体内容
     *
     * @param table 表格
     * @return this
     */
    public PdfBuilder body(CustomBody table) {
        this.document.add(table.getTable());
        return this;
    }


    /**
     * 输出文档对象
     *
     * @return 文档对象
     */
    @Override
    public PdfDocument build() {
        return this.document.getPdfDocument();
    }

    /**
     * 输出文件流对象
     *
     * @return 文件输出流
     */
    @Override
    public OutputStream buildStream() {
        return this.build().getWriter().getOutputStream();
    }
}
