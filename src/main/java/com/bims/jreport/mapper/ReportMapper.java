package com.bims.jreport.mapper;

import com.bims.jreport.jooq.db.tables.records.TTemplateRecord;
import com.bims.jreport.pojo.bo.ReportTemplateBo;
import com.bims.jreport.pojo.vo.ReportTemplateVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author wangrenjie
 */
@Mapper(componentModel = "spring")
public interface ReportMapper {

    TTemplateRecord bo2Rec(ReportTemplateBo bo);

    ReportTemplateVo bo2Vo(ReportTemplateBo bo);

    ReportTemplateBo vo2Bo(ReportTemplateVo bo);

    ReportTemplateBo rec2Bo(TTemplateRecord bo);

    List<ReportTemplateBo> recs2Bos(List<TTemplateRecord> bos);
}
