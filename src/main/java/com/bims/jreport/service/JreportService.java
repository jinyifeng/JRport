package com.bims.jreport.service;

import com.bims.jreport.pojo.bo.ReportTemplateBo;
import com.itextpdf.kernel.pdf.PdfDocument;

import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

/**
 * @author wangrenjie
 */
public interface JreportService {

    /**
     * 保存
     *
     * @param reportTemplateBo Bo
     * @return 是否成功
     */
    boolean save(ReportTemplateBo reportTemplateBo);

    /**
     * 删除
     *
     * @param templateId 模板ID
     * @return 是否成功
     */
    boolean delete(UUID templateId);

    /**
     * 删除所有
     *
     * @return 是否成功
     */
    boolean deleteAll(UUID formId);

    /**
     * 获取最新版
     *
     * @param formId 表单ID
     * @return Bo
     */
    ReportTemplateBo getLast(UUID formId);

    /**
     * 获得所有版本
     *
     * @param formId
     * @return
     */
    List<ReportTemplateBo> getVers(UUID formId);

    /**
     * 详情
     *
     * @param templateId 模板ID
     * @return BO
     */
    ReportTemplateBo get(UUID templateId);

    /**
     * 预览
     *
     * @param template 模板JSON
     */
    OutputStream preview(String template);

    /**
     * 获得文档模板
     *
     * @param templateJson 模板JSON
     * @return PDF文档
     */
    PdfDocument getDocumentTemplate(String templateJson);
}
