package com.bims.jreport.utils;

import com.bims.jreport.controller.RestResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * 国际化消息工具类
 */
@Slf4j
@Component
public class I18nMessagesUtil {

    @Autowired
    private MessageSource messageSource;


    /**
     * 根据消息编码获取相应的消息实体
     *
     * @param messageKey 消息编码
     * @return
     */
    public RestResult getFailedResponse(String messageKey) {
        return RestResult.newFailedInstance(messageKey, messageSource.getMessage(messageKey, null, LocaleContextHolder.getLocale()));
    }

    /**
     * 根据消息编码获取相应的消息实体
     *
     * @param messageKey 消息编码
     * @return
     */
    public RestResult getFailedResponse(String messageKey, Object[] params) {
        return RestResult.newFailedInstance(messageKey, messageSource.getMessage(messageKey, params, LocaleContextHolder.getLocale()));
    }

    /**
     * 根据消息编码返回相应的消息
     *
     * @param messageKey
     * @return
     */
    public String getMessage(String messageKey) {
        return messageSource.getMessage(messageKey, null, LocaleContextHolder.getLocale());
    }

}
