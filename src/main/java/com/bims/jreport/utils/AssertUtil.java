package com.bims.jreport.utils;


import com.bims.jreport.exception.LogicRunTimeException;

/**
 * 用于判断抛出业务异常
 * @author wangrenjie
 */
public class AssertUtil {

    public static void checkParam(boolean check, String code, String errmsg) {
        if (check) {
            throw new LogicRunTimeException(code, errmsg);
        }
    }


    public static void check(boolean check, String errMsgKey) {
        if (check) {
            throw new LogicRunTimeException(errMsgKey);
        }
    }

    public static void error(String errMsgKey) {
        throw new LogicRunTimeException(errMsgKey);
    }

}
