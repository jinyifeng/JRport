package com.bims.jreport.enums;

/**
 * @author wangrenjie
 */
public enum BindTypeEnum {
    Form,//表单
    Workflow,//工作流
    Exp,//公式
}
