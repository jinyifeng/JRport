package com.bims.jreport.enums;

/**
 * @author wangrenjie
 */

public enum BorderStyleEnum {
    DOUBLE,//双重
    SOLID,//单行实线
}
