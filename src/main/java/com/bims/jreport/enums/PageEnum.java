package com.bims.jreport.enums;

import com.itextpdf.kernel.geom.PageSize;

/**
 * @author wangrenjie
 */

public enum PageEnum {
    A4(PageSize.A4),
    ;
    private final PageSize pageSize;

    PageEnum(PageSize page) {
        this.pageSize = page;
    }

    public PageSize getPageSize() {
        return this.pageSize;
    }
}
