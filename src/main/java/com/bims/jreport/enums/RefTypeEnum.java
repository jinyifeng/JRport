package com.bims.jreport.enums;

/**
 * @author wangrenjie
 */
public enum RefTypeEnum {
    Image,//图片集
    Sheet,//工作簿
}
