package com.bims.jreport.enums;

/**
 * @author wangrenjie
 */
public enum FontStyleEnum {
    NORMAL,//正常
    ITALIC,//斜体
    UNDERLINE,//下划线
    BOLD,//加粗
}
