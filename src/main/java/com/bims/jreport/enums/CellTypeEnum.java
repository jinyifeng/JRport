package com.bims.jreport.enums;

/**
 * 单元格类型：text(文本), image(图片), qr(二维码)，sign(签名), field(数据字段), innerForm(内嵌表单), file(附件)。
 *
 * @author wangrenjie
 */

public enum CellTypeEnum {
    FIELD,//数据字段
    FILE,//附件
    IMAGE,//图片
    INNER_FORM,//内嵌表单
    QR,//二维码
    SIGN,//签名
    TEXT,//文本
}
